class Ship {
	public var energy: Float;
	public var maxCapacity: Float;
	public var drain(get, never): Float;
	public var charge(get, never): Float;
	public var storage: Vector2D<Material>;
	public var waste: Vector2D<Material>;
	public var furnace: Array<BurningMaterial>;
	public var drainDescription(get, never): Array<String>;

	public var temperature: Float;

	public var os: OS;
	public var delta: Float = 0;

	public function new(os: OS) {
		this.os = os;
		this.maxCapacity = Constants.ShipStartMaxCapacity;
		this.energy = 0.7 * this.maxCapacity;
		this.storage = new Vector2D(Constants.StorageSize, null);
		this.waste = new Vector2D(Constants.StorageSize, null);
		this.furnace = [];
		this.temperature = 3000;
	}

	public function get_drain(): Float {
		var d: Float = Constants.ShipDefaultEnergyDrain;
		d += this.os.surrounding.energyCost;
		return d;
	}

	public function get_drainDescription(): Array<String> {
		final descriptions = [];
		final charge = this.charge;
		descriptions.push('Burning: +${this.charge}/s');
		descriptions.push('Engine: -${Constants.ShipDefaultEnergyDrain}/s');
		final tractor = this.os.surrounding.energyCost;
		if (tractor > 0) descriptions.push('Tractor: -${tractor}/s');
		final net = this.charge - tractor - Constants.ShipDefaultEnergyDrain;
		descriptions.push("");
		descriptions.push('Net: ${net > 0 ? "+" : ""}${net}/s');
		return descriptions;
	}

	public function get_charge(): Float {
		var d: Float = 0;
		for (m in this.furnace) {
			d += m.material.energyPerSecond;
		}
		return d;
	}

	public function update(dt: Float) {
		this.delta += dt;
		this.energy -= dt * drain;

		var finished: Array<BurningMaterial> = [];
		for (bm in this.furnace) {
			final burnDT = Math.clampF(dt, 0, bm.burnDurationLeft);
			bm.burnDurationLeft -= burnDT;
			if (bm.burnDurationLeft <= 0) {
				finished.push(bm);
			}
			this.energy += burnDT * bm.material.energyPerSecond;
			this.temperature += burnDT * bm.material.temperature;
		}

		for (f in finished) {
			furnace.remove(f);
			onFinishBurning(f.material);
			// TODO: if waste material cannot be added to the storage, crash the OS
		}
	}

	public function canBurnMaterials(materials: Array<Material>): Bool {
		return !isFurnaceFull(materials.length);
	}

	public function burnMaterials(materials: Array<Material>): Bool {
		if (!canBurnMaterials(materials)) return false;
		// find and remove them from storage
		for (m in materials) {
			removeFromStorage(m);
			final bMaterial = new BurningMaterial(m);
			onBurned(m);
			this.furnace.push(bMaterial);
		}
		return true;
	}

	public function disposeMaterials(materials: Array<Material>) {
		for (m in materials) {
			removeFromStorage(m);
		}
	}

	public function addMaterial(m: Material): Bool {
		final points: Array<Point2i> = [];
		for (y in 0...this.storage.size.y) {
			for (x in 0...this.storage.size.x) {
				if (this.storage.get(x, y) != null) continue;
				points.push([x, y]);
			}
		}
		if (points.length == 0) {
			this.os.open_bsod("STORAGE_BUFFER_OVERFLOW", "STOP: 0xUNSTABLE_BO_0000037C");
			return false;
		}
		final pt = this.os.r.randomChoice(points);
		this.storage.set(pt.x, pt.y, m);
		return true;
	}

	function removeFromStorage(m: Material) {
		for (pt => material in storage.iterateYX()) {
			if (material == m) {
				storage.set(pt.x, pt.y, null);
				return;
			}
		}
	}

	function onBurned(m: Material) {
		energy += m.energyOnBurn;
	}

	function onFinishBurning(m: Material) {
		if (m.burned) return;
		energy += m.energyOnFinish;
		m.burned = true;
		this.addMaterial(m);
	}

	function isFurnaceFull(l: Int): Bool {
		return this.furnace.length + l > Constants.MaxFurance;
	}
}
