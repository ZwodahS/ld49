class VictoryScreen extends zf.Screen {
	// this prevents accidental click
	var delta: Float = 0;

	public function new() {
		super();

		final victoryText = new h2d.Text(Assets.bodyFont3x);
		victoryText.text = "As the ship begins to land, you remind yourself to find the person that made this Unstable OS.";
		victoryText.maxWidth = Globals.game.gameWidth - 50;
		victoryText.textAlign = Center;
		victoryText.setX(25).setY(Globals.game.gameHeight, AlignCenter);
		this.addChild(victoryText);
	}

	override public function update(dt: Float) {
		this.delta += dt;
	}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {
		// don't handle any event < 2 seconds into the screen
		if (delta < 2) return;
		switch (event.kind) {
			case ERelease:
				this.game.switchScreen(new GameScreen());
			default:
		}
	}

	override public function destroy() {}

	override public function resize(x: Int, y: Int) {}
}
