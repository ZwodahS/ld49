class BurningMaterial {
	public var material: Material;
	public var burnDurationLeft: Float;

	public function new(m: Material) {
		this.material = m;
		this.burnDurationLeft = m.burnDuration;
	}
}
