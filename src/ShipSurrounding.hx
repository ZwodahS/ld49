// we will assume ship is at 0, 0
// all material will spawn between 0 to 200
class ShipSurrounding {
	public var visibleMaterials: Array<MiningMaterial>;

	public var energyCost(get, never): Float;

	var world: OS;

	public function new(world: OS) {
		this.visibleMaterials = [];
		this.world = world;
	}

	public function update(dt: Float) {
		final mined: Array<MiningMaterial> = [];
		for (m in visibleMaterials) {
			if (!m.isBeaming) continue;
			m.x += m.unitVec.x * dt * Constants.MiningSpeed;
			m.y += m.unitVec.y * dt * Constants.MiningSpeed;
			if (m.unitVec.x > 0) {
				if (m.x >= 0) {
					mined.push(m);
					continue;
				}
			} else {
				if (m.x <= 0) {
					mined.push(m);
					continue;
				}
			}
			if (m.unitVec.y > 0) {
				if (m.y >= 0) mined.push(m);
			} else {
				if (m.y <= 0) mined.push(m);
			}
		}

		for (m in mined) {
			this.visibleMaterials.remove(m);
			this.world.ship.addMaterial(m.material);
		}
	}

	public function spawnNewMaterials(r: hxd.Rand) {
		final toRemove: Array<MiningMaterial> = [];
		for (m in visibleMaterials) {
			if (m.isBeaming) continue;
			toRemove.push(m);
		}

		for (m in toRemove) {
			this.visibleMaterials.remove(m);
		}

		for (i in 0...(Constants.MaxMining - this.visibleMaterials.length)) {
			final m = new MiningMaterial(Material.randomMaterial(r));
			this.visibleMaterials.push(m);
			m.x = r.randomWithinRange(-150, 150);
			m.y = r.randomWithinRange(-150, 150);
		}
	}

	public function get_energyCost(): Float {
		var numMining: Int = 0;
		for (m in this.visibleMaterials) {
			if (m.isBeaming) numMining += 1;
		}
		return numMining * 0.5;
	}
}
