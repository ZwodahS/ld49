class ShipEvent {
	public var timing: Float;
	public var onTrigger: (OS->Void);

	public function new(timing: Float, onTrigger: (OS->Void)) {
		this.timing = timing;
		this.onTrigger = onTrigger;
	}
}
