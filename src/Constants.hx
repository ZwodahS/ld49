/**
	Constants are constants value / magic numbers for the game.
	These should be set to be final.

	For the evil globals counterpart, see Globals.hx
	For the function counterpart, see Utils.hx
**/
class Constants {
	public static final Version: String = "0.1.2";

	public static final ColorBg = 0x14182e;

	public static final AppIconSize: Point2i = [64, 64];

	public static final ColorsGrey: Array<Color> = [0xffc2c2c2, 0xffa8a8a8, 0xff545454];
	public static final ColorsRed: Array<Color> = [0xfffe6d6d, 0xffb34f4f, 0xff803838];
	public static final ColorsBlue: Array<Color> = [0xff53bbf3, 0xff3e8ab3, 0xff2d6280];
	public static final ColorsYellow: Array<Color> = [0xfffafe62, 0xffb3b547, 0xff7e8033];
	public static final ColorsGreen: Array<Color> = [0xff6af07a, 0xff50b35c, 0xff398042];
	public static final ColorsWhite: Array<Color> = [0xffffffff];
	public static final ColorsOrange: Array<Color> = [0xfffeba6d];

	/**
		Gameplay constant
	**/
	public static final ShipStartMaxCapacity = 300;

	public static final ShipDefaultEnergyDrain = 3;

	public static final StorageSize: Point2i = [5, 5];

	public static final MaterialSize: Point2i = [32, 32];

	public static final RedEnergyPerSecond = 1;
	public static final RedOnBurned = 10;
	public static final RedOnFinish = 0;
	public static final RedBurnDuration = 10;
	public static final RedTemperature = 10;

	public static final BlueEnergyPerSecond = 1;
	public static final BlueOnBurned = 0;
	public static final BlueOnFinish = 10;
	public static final BlueBurnDuration = 10;
	public static final BlueTemperature = -10;

	public static final GreenEnergyPerSecond = 1;
	public static final GreenOnBurned = 0;
	public static final GreenOnFinish = 0;
	public static final GreenBurnDuration = 10;
	public static final GreenTemperature = 0;

	public static final NotificationMoveSpeed = 70;
	public static final ScreenLineMoveSpeed = 600;
	public static final ScreenLineChance = 25;

	public static final MaxFurance = 10;

	public static final EngineTemperatureMin = 2000;
	public static final EngineTemperatureMax = 4000;

	public static final MiningSpeed = 20;
	public static final MiningRespawnDelay = 10;
	public static final MaxMining = 14;

	public static final TotalTime: Int = 60 * 3;

	public static final SignalDegradeCheckDelay: Int = 10;
	public static final SignalDegradeChance: Int = 15;

	public static final ButtonSwapChance: Int = 10;
}
