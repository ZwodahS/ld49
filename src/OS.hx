import ui.Notification;

enum OSState {
	FirstBoot;
	Rebooting;
	Active;
	ShutingDown;
	BlueScreen;
	Gameover;
}

class OS extends h2d.Layers {
	public static final NoMoreEnergy = "nomoreenergy";
	public static final TooMuchEnergy = "toomuchenergy";

	public var notifications: Array<Notification>;
	public var notificationLayer: h2d.Layers;

	public var battery: h2d.Bitmap;
	public var signal: h2d.Anim;

	public var oshud: h2d.Layers;
	public var osButtons: h2d.Layers;
	public var appLayer: h2d.Layers;

	public var buttonsLayer: h2d.Object;
	public var buttonsBg: h2d.Bitmap;
	public var homeButton: zf.ui.Button;
	public var rebootButton: zf.ui.Button;

	// stores the ship information
	public var ship: Ship;
	// stores the player's health condition
	public var healthCondition: HealthCondition;
	// store the ship surrounding
	public var surrounding: ShipSurrounding;
	public var respawnDelay: Float = 0;

	public var activeApp: App = null;

	public var end: String = null;
	public var r: hxd.Rand = null;
	// between 0 - 3. It will slowly drop
	public var signalStrength(default, set): Int = 0;

	public var bootScreen: Boot = null;
	public var bsod: BSOD = null;

	public var state: OSState = Active;

	public var events: Array<ShipEvent>;

	public var screenLinesLayer: h2d.Layers;
	public var screenLines: Array<ScreenLine>;

	public function new() {
		super();
		this.r = new hxd.Rand(Random.int(0, zf.Constants.SeedMax));
		this.notifications = [];
		this.screenLines = [];

		this.ship = new Ship(this);
		for (i in 0...10) {
			ship.addMaterial(Material.randomMaterial(this.r));
		}
		this.surrounding = new ShipSurrounding(this);

		this.events = [];
		// add the default events
		this.events.push(new ShipEvent(Constants.TotalTime - 120, function(os: OS) {
			this.ship.maxCapacity = 250;
			showNotification("warning", "Power Cell 1 malfunction. Energy Capacity drop to 250");
		}));

		this.events.push(new ShipEvent(Constants.TotalTime - 60, function(os: OS) {
			this.ship.maxCapacity = 200;
			showNotification("warning", "Power Cell 2 malfunction. Energy Capacity drop to 200");
		}));

		this.events.push(new ShipEvent(Constants.TotalTime - 30, function(os: OS) {
			this.ship.maxCapacity = 150;
			showNotification("warning", "Power Cell 3 malfunction. Energy Capacity drop to 150");
		}));

		for (t in [30, 60, 90, 120, 150]) {
			this.events.push(new ShipEvent(t, function(os: OS) {
				final spike = (this.r.randomWithinRange(-1500, 1500) / 10);
				this.ship.temperature += spike;
				if (spike > 0) {
					showNotification("warning", 'Engine Temperature Spiked: +${spike}');
				} else {
					showNotification("warning", 'Engine Temperature Dropped: ${spike}');
				}
			}));
		}

		this.events.sort(function(s1: ShipEvent, s2: ShipEvent) {
			return Std.int(s1.timing - s2.timing);
		});

		this.healthCondition = new HealthCondition();
		this.add(this.oshud = new h2d.Layers(), 100);
		this.add(this.osButtons = new h2d.Layers(), 100);
		this.add(this.appLayer = new h2d.Layers(), 150);
		this.add(this.notificationLayer = new h2d.Layers(), 200);
		this.add(this.screenLinesLayer = new h2d.Layers(), 300);
		this.appLayer.y = 40;
		this.oshud.add(this.battery = Assets.packed.assets.get("battery").getBitmap(), 50);
		this.oshud.add(this.signal = Assets.packed.assets.get("signal").getAnim(), 50);
		this.signal.currentFrame = 2;
		this.signal.pause = true;

		setupOsButtons();

		closeActiveApp();
		reboot(true);
	}

	public function set_signalStrength(s: Int) {
		s = Math.clampI(s, 0, 3);
		this.signalStrength = s;
		this.signal.currentFrame = this.signalStrength;
		if (this.signalStrength == 0) {
			showNotification("warning", "Connection to Ship lost. Reboot System to restore connection");
		}
		return this.signalStrength;
	}

	public function showNotification(type: String, message: String) {
		final notification = new Notification(type, message);
		this.notifications.push(notification);
	}

	function updateNotifications(dt: Float) {
		if (this.notifications.length == 0) return;
		// we only show one at a time
		final notification = this.notifications[0];
		switch (this.notifications[0].state) {
			case Hidden:
				final size = notification.getSize();
				notification.moveTo = 10;
				notification.y = -size.height - 10;
				notification.x = 10;
				this.notificationLayer.addChild(notification);
			case Shown:
				final size = notification.getSize();
				notification.delay += dt;
				if (notification.delay > 3) {
					notification.moveTo = -size.height - 10;
				}
			case Close:
				notification.remove();
				this.notifications.remove(notification);
			default:
		}
		notification.update(dt);
	}

	public function resize(x: Int, y: Int) {
		this.battery.setX(x, AnchorRight, 10).setY(5);
		this.signal.putOnLeft(this.battery, [5, 0]);
		if (this.activeApp != null) activeApp.resize(x, y);

		this.buttonsBg.width = x;

		this.osButtons.setY(y, AnchorBottom, 0);
		this.buttonsLayer.setX(x, AlignCenter, 0);
	}

	public var signalCheckDelta: Float = 0;

	public function update(dt: Float) {
		if (this.bsod != null) {
			this.bsod.update(dt);
			if (this.bsod.done) {
				this.bsod.remove();
				this.bsod = null;
				reboot(false, true);
			}
		}
		if (this.bootScreen != null) {
			this.bootScreen.update(dt);
			if (this.bootScreen.done) {
				this.bootScreen.remove();
				this.bootScreen = null;
				this.state = Active;
			}
		}
		if (this.bsod == null && this.bootScreen == null) {
			this.updateNotifications(dt);
			this.updateScreenLines(dt);
			if (this.state == ShutingDown || this.end != null) {
				if (this.notifications.length == 0) {
					shutdown();
				}
			}
		}

		if (end != null) return;

		// Randomly degrade signal strength every 10 seconds.
		this.signalCheckDelta += dt;
		if (this.signalCheckDelta >= Constants.SignalDegradeCheckDelay) {
			this.signalCheckDelta -= Constants.SignalDegradeCheckDelay;
			if (this.r.randomChance(Constants.SignalDegradeChance)) {
				this.signalStrength -= 1;
			}

			if (true || this.r.randomChance(Constants.ScreenLineChance)) addScreenLine();
		}

		final energyStart = this.ship.energy;
		final tempStart = this.ship.temperature;
		this.ship.update(dt);
		this.healthCondition.update(dt);
		this.surrounding.update(dt);

		this.respawnDelay -= dt;

		if (respawnDelay <= 0) {
			this.surrounding.spawnNewMaterials(this.r);
			this.respawnDelay += Constants.MiningRespawnDelay;
		}

		handleScriptedEvents();

		final energyEnd = this.ship.energy;
		final tempEnd = this.ship.temperature;
		{
			final lowThreshold = Std.int(this.ship.maxCapacity * 0.2);
			final highThreshold = Std.int(this.ship.maxCapacity * 0.8);
			if (energyStart > lowThreshold && energyEnd <= lowThreshold) {
				this.showNotification("warning", 'Low Energy');
			} else if (energyStart < highThreshold && energyEnd >= highThreshold) {
				this.showNotification("warning", 'Energy capacity reaching limit');
			}
		}
		{
			final lowThreshold = Constants.EngineTemperatureMin + 500;
			final highThreshold = Constants.EngineTemperatureMax - 500;
			if (tempStart > lowThreshold && tempEnd <= lowThreshold) {
				this.showNotification("warning", 'Low Temperature');
			} else if (tempStart < highThreshold && tempEnd >= highThreshold) {
				this.showNotification("warning", 'High Temperature');
			}
		}

		if (this.activeApp != null && this.signalStrength > 0) {
			if (this.activeApp.scaleX != 1) {
				this.activeApp.scaleX += dt * 3.0;
				this.activeApp.scaleY += dt * 3.0;
				if (this.activeApp.scaleX >= 1) {
					this.activeApp.scaleX = 1;
					this.activeApp.scaleY = 1;
				}
				if (this.activeApp.scaleX == 1) {
					this.activeApp.x = 0;
					this.activeApp.y = 0;
				} else {
					final w = this.activeApp.scaleX * Globals.game.gameWidth;
					final h = this.activeApp.scaleY * (Globals.game.gameHeight - 40);
					this.activeApp.x = (Globals.game.gameWidth - w) / 2;
					this.activeApp.y = (Globals.game.gameHeight - 40 - h) / 2;
				}
			}
			this.activeApp.update(dt);
		}

		if (this.bootScreen == null) { // ensure that while in boot, this don't check.
			checkVictory();
			checkGameover();
		}
	}

	function checkVictory() {
		if (this.ship.delta >= Constants.TotalTime) {
			end = "victory";
			this.state = ShutingDown;
			this.showNotification("victory", "Welcome home Captain");
		}
	}

	function checkGameover() {
		if (this.ship.energy <= 0) {
			this.state = ShutingDown;
			this.end = NoMoreEnergy;
			this.showNotification("gameover",
				"Power Level dropped to critical level. Shutting down everything");
		} else if (this.ship.energy > this.ship.maxCapacity) {
			this.state = ShutingDown;
			this.end = TooMuchEnergy;
			this.showNotification("gameover", "Engine overloaded. Engine exploding in 5");
			this.showNotification("gameover", "Engine overloaded. Engine exploding in 1");
		}
	}

	public function switchToApp(app: App) {
		if (this.activeApp != null) {
			closeActiveApp(false);
		}

		this.activeApp = app;
		this.appLayer.addChild(app);
		app.scaleX = 0;
		app.scaleY = 0;
		app.x = Globals.game.gameWidth / 2;
		app.y = (Globals.game.gameHeight - 40) / 2;
	}

	public function closeActiveApp(openHome: Bool = true) {
		if (this.activeApp != null) {
			this.activeApp.remove();
			this.activeApp = null;
		}

		if (openHome) switchToApp(new HomeApp(this));
	}

	function setupOsButtons() {
		this.buttonsBg = Assets.packed.assets.get("white").getBitmap();
		this.buttonsBg.width = Globals.game.gameWidth + 5;
		this.buttonsBg.height = 50;
		this.buttonsBg.color.setColor(Constants.ColorsGrey[2]);
		this.osButtons.addChild(buttonsBg);

		this.buttonsLayer = new h2d.Object();

		this.buttonsLayer.addChild(this.homeButton = zf.ui.GenericButton.fromTileColors(Assets.packed.assets.get("btn:home")
			.getTile(), [Constants.ColorsGrey[0]]));
		this.homeButton.y = 8;
		this.homeButton.onClick = function(button: Int) {
			closeActiveApp();
		}

		this.buttonsLayer.addChild(this.rebootButton = zf.ui.GenericButton.fromTileColors(Assets.packed.assets.get("btn:reboot")
			.getTile(), [Constants.ColorsGrey[0]]));

		this.rebootButton.putOnRight(this.homeButton, [80, 0]);
		this.rebootButton.onClick = function(button: Int) {
			reboot();
		}
		this.osButtons.addChild(buttonsLayer);
	}

	function handleScriptedEvents() {
		while (this.events.length > 0 && this.events[0].timing <= this.ship.delta) {
			final event = this.events[0];
			this.events.remove(event);
			event.onTrigger(this);
		}
	}

	public function reboot(firstBoot: Bool = false, force: Bool = false) {
		if (this.state != Active && !force) return;
		closeActiveApp(true);
		this.bootScreen = new Boot();

		if (firstBoot) {
			this.state = FirstBoot;
		} else {
			if (this.end == null) this.state = Rebooting;
		}
		this.signalStrength = 3;
		for (sl in this.screenLines) {
			sl.remove();
		}
		this.screenLines.clear();
		this.add(bootScreen, 2000);
	}

	public function open_bsod(str: String, code: String) {
		if (this.state == BlueScreen) return;
		this.bsod = new BSOD(str, code);
		this.add(bsod, 4000);
		this.state = BlueScreen;
	}

	function shutdown() {
		this.state = Gameover;
	}

	function addScreenLine() {
		var height = 3;
		var sl = new ScreenLine(height, this.r);
		screenLines.push(sl);
		this.screenLinesLayer.addChild(sl);
	}

	function updateScreenLines(dt: Float) {
		for (sl in this.screenLines) {
			sl.update(dt);
		}
	}
}
