class Boot extends h2d.Layers {
	public var bootDelay: Float = 3;

	var delta: Float = 0;

	public var done = false;

	var logo: h2d.Bitmap;

	public function new() {
		super();
		setupUI();
	}

	function setupUI() {
		final bg = Assets.fromColor(Constants.ColorsGrey[2], Globals.game.gameWidth, Globals.game.gameHeight);
		this.addChild(bg);
		this.logo = Assets.packed.assets.get("bootlogo").getBitmap();
		this.logo.scale(2);
		logo.setX(Globals.game.gameWidth, AlignCenter).setY(Globals.game.gameHeight, AlignCenter);
		this.addChild(logo);
		this.logo.visible = false;
	}

	public function update(dt: Float) {
		delta += dt;
		if (delta > .5) this.logo.visible = true;
		if (delta > bootDelay) this.done = true;
	}
}
