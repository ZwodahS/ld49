enum abstract MaterialType(String) from String to String {
	public var Red = "red";
	public var Blue = "blue";
	public var Green = "green";
}

class Material extends h2d.Object {
	public var burned(default, set): Bool = false;
	public var type(default, null): MaterialType;
	public var description(get, never): String;
	public var burnDuration(get, never): Float;
	public var energyPerSecond(get, never): Float;
	public var energyOnBurn(get, never): Float;
	public var energyOnFinish(get, never): Float;
	public var temperature(get, never): Float;

	var block: h2d.Anim;

	public function new(type: MaterialType) {
		super();
		this.type = type;
		this.burned = false;

		updateRender();
	}

	public function set_burned(b: Bool): Bool {
		this.burned = b;
		updateRender();
		return this.burned;
	}

	function updateRender() {
		if (this.block == null) {
			final id = '${type}block';
			final assets = Assets.packed.assets.get(id);
			this.block = assets.getAnim();
			this.block.pause = true;
			this.addChild(this.block);
		}

		if (this.burned) {
			this.block.currentFrame = 1;
		} else {
			this.block.currentFrame = 0;
		}
	}

	public static function randomMaterial(r: hxd.Rand): Material {
		switch (r.randomInt(3)) {
			case 0:
				return new Material(Red);
			case 1:
				return new Material(Blue);
			case 2:
				return new Material(Green);
			default:
				return null;
		}
	}

	public function get_description(): String {
		if (this.burned) {
			return ["Waste Material", "Burn for a long time, but does nothing",].join("\n");
		} else {
			var descriptions: Array<String> = [];
			descriptions.push('Each unit of Material provide: ');
			switch (this.type) {
				case Red:
					descriptions.push('- ${Constants.RedOnBurned} Power when burned');
					descriptions.push('- ${Constants.RedEnergyPerSecond} Power/Sec for ${Constants.RedBurnDuration}s');
					descriptions.push('- ${Constants.RedTemperature}K/Sec');
				case Blue:
					descriptions.push('- ${Constants.BlueEnergyPerSecond} Power/Sec for ${Constants.BlueBurnDuration}s');
					descriptions.push('- ${Constants.BlueOnFinish} Power when finish burning');
					descriptions.push('- ${Constants.BlueTemperature}K/Sec');
				case Green:
					descriptions.push('- ${Constants.GreenEnergyPerSecond} Power/Sec for ${Constants.GreenBurnDuration}s');
				default:
			}
			return descriptions.join("\n");
		}
	}

	public function get_burnDuration(): Float {
		if (this.burned) {
			return 60;
		} else {
			switch (this.type) {
				case Red:
					return Constants.RedBurnDuration;
				case Blue:
					return Constants.BlueBurnDuration;
				case Green:
					return Constants.GreenBurnDuration;
				default:
					return 0;
			}
		}
	}

	public function get_energyPerSecond(): Float {
		if (this.burned) {
			return 0;
		} else {
			switch (this.type) {
				case Red:
					return Constants.RedEnergyPerSecond;
				case Blue:
					return Constants.BlueEnergyPerSecond;
				case Green:
					return Constants.GreenEnergyPerSecond;
				default:
					return 0;
			}
		}
	}

	public function get_energyOnBurn(): Float {
		if (this.burned) {
			return 0;
		} else {
			switch (this.type) {
				case Red:
					return Constants.RedOnBurned;
				case Blue:
					return Constants.BlueOnBurned;
				case Green:
					return Constants.GreenOnBurned;
				default:
					return 0;
			}
		}
	}

	public function get_energyOnFinish(): Float {
		if (this.burned) {
			return 0;
		} else {
			switch (this.type) {
				case Red:
					return Constants.RedOnFinish;
				case Blue:
					return Constants.BlueOnFinish;
				case Green:
					return Constants.GreenOnFinish;
				default:
					return 0;
			}
		}
	}

	public function get_temperature(): Float {
		if (this.burned) {
			return 0;
		} else {
			switch (this.type) {
				case Red:
					return Constants.RedTemperature;
				case Blue:
					return Constants.BlueTemperature;
				case Green:
					return Constants.GreenTemperature;
				default:
					return 0;
			}
		}
	}
}
