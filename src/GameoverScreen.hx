class GameoverScreen extends zf.Screen {
	// this prevents accidental click
	var delta: Float = 0;

	public function new() {
		super();

		final gameover = new h2d.Text(Assets.bodyFont3x);
		gameover.text = "You fail to reach the home planet and are forever lost in space.";
		gameover.maxWidth = Globals.game.gameWidth - 50;
		gameover.setX(25).setY(Globals.game.gameHeight, AlignCenter);
		gameover.textAlign = Center;
		this.addChild(gameover);
	}

	override public function update(dt: Float) {
		this.delta += dt;
	}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {
		// don't handle any event < 2 seconds into the screen
		if (delta < 2) return;
		switch (event.kind) {
			case ERelease:
				this.game.switchScreen(new GameScreen());
			default:
		}
	}

	override public function destroy() {}

	override public function resize(x: Int, y: Int) {}
}
