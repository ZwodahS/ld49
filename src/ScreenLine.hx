class ScreenLine extends h2d.Object {
	public var moveTo: Float;

	var r: hxd.Rand;

	public function new(height: Int, r: hxd.Rand) {
		super();
		this.r = r;
		setupUI(height);
		this.y = this.moveTo = 40;
	}

	function setupUI(height: Int) {
		final line = Assets.fromColor(Constants.ColorsGrey[1], Globals.game.gameWidth, height);
		this.addChild(line);
		final interactive = new h2d.Interactive(Globals.game.gameWidth, height + 3, this);
		interactive.y = -1;
		interactive.propagateEvents = false;
		interactive.cursor = Default;
	}

	public function update(dt: Float) {
		if (this.y == this.moveTo) {
			this.moveTo = this.r.randomWithinRange(40, 500);
		} else {
			if (this.y > this.moveTo) {
				final move = Math.clampF(Constants.ScreenLineMoveSpeed * dt, 0, this.y - this.moveTo);
				this.y -= move;
			} else if (this.y < this.moveTo) {
				final move = Math.clampF(Constants.ScreenLineMoveSpeed * dt, 0, this.moveTo - this.y);
				this.y += move;
			}
		}
	}
}
