class GameScreen extends zf.Screen {
	var world: OS;

	var exiting = false;
	var waitDelay: Float = 0;
	var line: h2d.Object = null;

	public function new() {
		super();
		this.addChild(this.world = new OS());
	}

	override public function update(dt: Float) {
		// easier for me to animate here than in world.
		if (!this.exiting) {
			this.world.update(dt);
			if (this.world.end != null && this.world.state == Gameover) {
				if (this.world.end == "victory") {
					this.game.switchScreen(new VictoryScreen());
				} else {
					this.game.switchScreen(new GameoverScreen());
				}
			}
		} else {
			if (this.world.scaleY == 0) {
				this.waitDelay += dt;
				this.line.alpha = 1 - this.waitDelay;
			} else {
				this.world.scaleY -= dt * 3.5;
				if (this.world.scaleY < 0) this.world.scaleY = 0;
				this.world.setY((Globals.game.gameHeight
					- (this.world.scaleY * Globals.game.gameHeight)) / 2);
				if (this.world.scaleY == 0) {
					this.line = Assets.fromColor(Constants.ColorsWhite[0], Globals.game.gameWidth, 1);
					this.addChild(line);
					line.y = this.world.y;
				}
			}
		}
	}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {}

	override public function destroy() {}

	override public function resize(x: Int, y: Int) {
		this.world.resize(x, y);
	}

	override public function beginScreenExit() {
		this.exiting = true;
	}

	override public function doneExiting() {
		return this.world.scaleY == 0 && waitDelay > 1;
	}
}
