class MiningMaterial extends h2d.Object {
	public var material: Material;
	public var isBeaming(default, set): Bool = false;
	public var unitVec: Point2f = null;
	public var border: h2d.Anim;

	public function new(m: Material) {
		super();
		this.material = m;
		var obj = new h2d.Object();
		obj.addChild(m);
		this.addChild(obj);
		obj.x = -8;
		obj.y = -8;
		this.scale(2);

		final interactive = new h2d.Interactive(16, 16, this);
		interactive.cursor = Default;
		interactive.onClick = function(e: hxd.Event) {
			onClick(this);
		}
		interactive.x = -8;
		interactive.y = -8;
	}

	public function set_isBeaming(b: Bool): Bool {
		this.isBeaming = b;
		if (this.isBeaming && this.border == null) {
			this.addChild(this.border = Assets.packed.assets.get("tractorbeam").getAnim());
			this.border.x = -9;
			this.border.y = -9;
			this.border.speed = 4;
		}
		return this.isBeaming;
	}

	dynamic public function onClick(m: MiningMaterial) {}
}
