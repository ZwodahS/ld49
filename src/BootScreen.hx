class BootScreen extends zf.Screen {
	public function new() {
		super();
	}

	override public function update(dt: Float) {
		this.game.switchScreen(new GameScreen());
	}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {}

	override public function destroy() {}
}
