package ui;

class TextBox extends h2d.Object {
	var bg: h2d.Object;

	public var text: h2d.Text;

	public function new(message: String, width: Int, height: Int) {
		super();
		this.addChild(this.bg = Assets.boxFactory.make([width, height]));
		this.addChild(this.text = new h2d.Text(Assets.bodyFont2x));

		this.text.text = message;
		this.text.maxWidth = width - 8;
		this.text.x = 4;
		this.text.y = 4;
	}
}
