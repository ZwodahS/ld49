package ui;

class TemperatureBar extends h2d.Object {
	var activeBar: zf.ui.Bar;

	var world: OS;

	public function new(world: OS) {
		super();
		this.world = world;
		this.addChild(Assets.fromColor(Constants.ColorsRed[2], 360, 32));
		this.addChild(this.activeBar = new zf.ui.Bar(EmptyBar, Constants.ColorsRed[1], null, 0x00000000,
			360, 32, Assets.packed.assets.get("white").getTile()));

		var border: h2d.Drawable = null;
		this.addChild(border = Assets.boxFactory.make([360, 32]));
		border.color.setColor(Constants.ColorsRed[0]);
	}

	public function update(dt: Float) {
		final ship = this.world.ship;
		// @formatter:off
		this.activeBar.width = (
			360 * (
				(ship.temperature - Constants.EngineTemperatureMin) /
				(Constants.EngineTemperatureMax - Constants.EngineTemperatureMin)
			)
		);
	}
}

