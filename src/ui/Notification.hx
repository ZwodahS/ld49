package ui;

enum NotificationState {
	Hidden;
	AnimatingShow;
	Shown;
	AnimatingHide;
	Close;
}

class Notification extends h2d.Object {
	var bg: h2d.Object;
	var border: h2d.Object;

	public var text: h2d.HtmlText;

	public var moveTo: Float = 0;

	public var state: NotificationState;

	public var delay: Float = 0;

	public function new(type: String, message: String) {
		super();
		this.state = Hidden;
		final width = Globals.game.gameWidth - 20;
		this.text = new h2d.HtmlText(Assets.bodyFont2x);
		final actualMessages: Array<String> = [];
		switch (type) {
			case "warning":
				actualMessages.push("Warning".font(Constants.ColorsYellow[0]));
			case "victory":
				actualMessages.push("Home sweet home".font(Constants.ColorsGreen[0]));
			case "gameover":
				actualMessages.push("It's the end".font(Constants.ColorsRed[0]));
			default:
				actualMessages.push("Warning".font(Constants.ColorsYellow[0]));
		}
		actualMessages.push("");
		actualMessages.push(message);

		this.text.text = actualMessages.join("<br/>");
		this.text.maxWidth = width - 8;
		this.text.x = 4;
		this.text.y = 4;
		final size = this.text.getSize();
		final height = size.height + 10;

		this.addChild(this.bg = Assets.fromColor(Constants.ColorsGrey[2], Std.int(width), Std.int(height)));
		this.addChild(this.border = Assets.boxFactory.make([Std.int(width), Std.int(height)]));
		this.addChild(this.text);
	}

	public function update(dt: Float) {
		if (this.y > this.moveTo) {
			final move = Math.clampF(Constants.NotificationMoveSpeed * dt, 0, this.y - this.moveTo);
			this.state = AnimatingHide;
			this.y -= move;
		} else if (this.y < this.moveTo) {
			final move = Math.clampF(Constants.NotificationMoveSpeed * dt, 0, this.moveTo - this.y);
			this.state = AnimatingShow;
			this.y += move;
		} else {
			switch (this.state) {
				case AnimatingShow:
					this.state = Shown;
				case AnimatingHide:
					this.state = Close;
				default:
			}
		}
	}
}
