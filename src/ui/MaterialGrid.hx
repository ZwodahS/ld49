package ui;

import Material;

class Grid extends h2d.Layers {
	public var position: Point2i;
	public var material(default, set): Material;

	public var selected(default, set): Bool = false;

	var selectedHighlight: h2d.Bitmap;

	public function new() {
		super();

		final size = Constants.MaterialSize;
		this.selectedHighlight = Assets.fromColor(Constants.ColorsYellow[0], Std.int(size.x / 2),
			Std.int(size.y / 2));
		this.selectedHighlight.alpha = .3;
		this.selectedHighlight.visible = false;
		this.add(this.selectedHighlight, 10);

		final interactive = new h2d.Interactive(size.x, size.y, this);
		interactive.cursor = Default;
		interactive.onClick = function(e: hxd.Event) {
			onClick(this);
		}

		this.scale(2);
	}

	public function set_selected(b: Bool): Bool {
		if (this.material == null) return false;
		this.selected = b;
		this.selectedHighlight.visible = this.selected;
		return this.selected;
	}

	public function set_material(m: Material): Material {
		if (this.material == m) return m;
		if (this.material != null) this.material.remove();
		this.material = m;
		if (this.material != null) this.add(this.material, 0);
		this.selected = false;
		return this.material;
	}

	dynamic public function onClick(g: Grid) {}
}

class MaterialGrid extends h2d.Layers {
	public var storage: Vector2D<Material>;

	var grid: Vector2D<Grid>;

	var currentlySelected: Array<Grid>;

	public var selectedMaterials(get, never): Array<Material>;

	public function new(storage: Vector2D<Material>) {
		super();
		this.storage = storage;

		this.grid = new Vector2D<Grid>(this.storage.size, null);
		for (y in 0...this.grid.size.y) {
			for (x in 0...this.grid.size.x) {
				final g = new Grid();
				this.grid.set(x, y, g);
				g.x = 0 + (x * (Constants.MaterialSize.x + 2));
				g.y = 0 + (y * (Constants.MaterialSize.y + 2));
				g.position = [x, y];
				g.onClick = onClick;
				this.add(g, 1);
			}
		}

		syncStorage();
	}

	function onClick(g: Grid) {
		if (this.currentlySelected == null) {
			select(g);
		} else {
			final selected = this.currentlySelected.contains(g);
			deselect();
			if (!selected) select(g);
		}
	}

	function select(g: Grid) {
		if (g == null || g.material == null) return;
		this.currentlySelected = [];
		final materials: Array<Material> = [];

		materials.push(g.material);
		g.selected = true;
		this.currentlySelected.push(g);

		var ind = 0;
		while (ind < this.currentlySelected.length) {
			var current = this.currentlySelected[ind];
			final adj = this.grid.getAdjacent(current.position.x, current.position.y);
			for (a in adj) {
				if (a.material != null
					&& a.material.burned == g.material.burned
					&& a.material.type == g.material.type
					&& !a.selected) {
					this.currentlySelected.push(a);
					a.selected = true;
					materials.push(a.material);
				}
			}
			ind += 1;
		}
		onSelect(materials);
	}

	public function get_selectedMaterials(): Array<Material> {
		if (this.currentlySelected == null) return [];
		return [for (m in this.currentlySelected) m.material];
	}

	public function deselect() {
		for (g in this.currentlySelected) {
			g.selected = false;
		}
		this.currentlySelected = null;
		onSelect(null);
	}

	public function syncStorage() {
		for (y in 0...this.grid.size.y) {
			for (x in 0...this.grid.size.x) {
				this.grid.get(x, y).material = this.storage.get(x, y);
			}
		}
	}

	dynamic public function onSelect(materials: Array<Material>) {}
}
