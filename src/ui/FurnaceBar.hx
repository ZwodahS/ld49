package ui;

class FurnaceBar extends h2d.Object {
	var world: OS;
	var burningMaterials: Array<BurningMaterial>;
	var materialObjects: Array<h2d.Object>;

	public function new(world: OS) {
		super();
		this.world = world;
		this.burningMaterials = [];
		this.materialObjects = [];

		this.addChild(Assets.fromColor(Constants.ColorsOrange[2], 360, 32));

		var border: h2d.Drawable = null;
		this.addChild(border = Assets.boxFactory.make([360, 32]));
		border.color.setColor(Constants.ColorsOrange[0]);
	}

	public function update(dt: Float) {
		// we will do a trick here. Because we always add to furnace in a literal fashion,
		// this means that if length is the same, and start end item is the same, nothing has changed.
		final ship = this.world.ship;

		// check length first
		if (this.burningMaterials.length == 0 && ship.furnace.length == 0) return;
		if (this.burningMaterials.length != ship.furnace.length
			|| this.burningMaterials[0] != ship.furnace[0]
			|| this.burningMaterials[this.burningMaterials.length
				- 1] != ship.furnace[ship.furnace.length - 1]) {
			// sync up
			for (m in this.burningMaterials) {
				m.material.remove();
			}
			for (mo in this.materialObjects) {
				mo.remove();
			}
			this.burningMaterials.clear();
			this.materialObjects.clear();
			for (ind => m in ship.furnace) {
				this.burningMaterials.push(m);
				var o = new h2d.Object();
				o.addChild(m.material);
				this.addChild(o);
				o.x = 4 + (ind * 20);
				o.y = 8;
				this.materialObjects.push(o);
			}
		}
	}
}
