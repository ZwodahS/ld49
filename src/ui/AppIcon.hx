package ui;

class AppIcon extends zf.ui.Button {
	var icon: h2d.Bitmap;
	var nameLabel: h2d.Text;

	var world: OS;

	public function new(world: OS, name: String, id: String) {
		super(Constants.AppIconSize.x, Constants.AppIconSize.y);
		this.addChild(this.nameLabel = new h2d.Text(Assets.bodyFont1x));
		this.addChild(this.icon = Assets.packed.assets.get(id).getBitmap());
		this.icon.scale(2);
		this.nameLabel.text = name;
		this.nameLabel.putBelow(this.icon);
		this.nameLabel.maxWidth = Constants.AppIconSize.x;
		this.nameLabel.textAlign = Center;
	}
}
