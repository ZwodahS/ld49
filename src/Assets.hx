/**
	Assets is used to store loaded assets
**/

import zf.Assets.LoadedSpritesheet;

class Assets {
	public static var packed: LoadedSpritesheet;

	public static var bodyFonts: Map<Int, h2d.Font>;
	public static var bodyFont1x: h2d.Font;
	public static var bodyFont2x: h2d.Font;
	public static var bodyFont3x: h2d.Font;
	public static var bodyFont4x: h2d.Font;
	public static var bodyFont5x: h2d.Font;
	public static var defaultFont: h2d.Font;

	public static var boxFactory: zf.ui.TileBoxFactory;

	public static function fromColor(color: Color, width: Int, height: Int): h2d.Bitmap {
		final bm = new h2d.Bitmap(Assets.packed.assets.get("white").getTile());
		bm.width = width;
		bm.height = height;
		bm.color.setColor(color);
		return bm;
	}
}
