package apps;

class MetricsApp extends App {
	var energyLabel: h2d.Text;
	var energyBar: EnergyBar;
	var energyDrain: Float;

	var tempLabel: h2d.Text;
	var tempBar: TemperatureBar;

	var furnaceLabel: h2d.Text;
	var furnaceBar: FurnaceBar;

	var details: h2d.Text;

	public function new(os: OS) {
		super(os);
		setupUI();
	}

	function setupUI() {
		final gameWidth = Globals.game.gameWidth;

		final title = new h2d.Text(Assets.bodyFont5x);
		title.text = "Metrics";
		title.setX(Globals.game.gameWidth, AlignCenter).setY(10);
		this.addChild(title);

		this.addChild(this.energyBar = new EnergyBar(this.os));
		this.energyBar.setX(Globals.game.gameWidth, AlignCenter);

		this.energyLabel = new h2d.Text(Assets.bodyFont2x);
		this.energyLabel.putBelow(title, [0, 15]).setX(this.energyBar.x);
		this.energyLabel.text = "Power Level";
		this.energyLabel.textColor = Constants.ColorsGrey[0];
		this.addChild(this.energyLabel);

		this.energyBar.putBelow(this.energyLabel, [0, 5]);

		this.addChild(this.tempBar = new TemperatureBar(this.os));

		this.tempLabel = new h2d.Text(Assets.bodyFont2x);
		this.tempLabel.putBelow(this.energyBar, [0, 10]);
		this.tempLabel.text = "Temperature";
		this.tempLabel.textColor = Constants.ColorsGrey[0];
		this.addChild(this.tempLabel);

		this.tempBar.putBelow(this.tempLabel, [0, 5]);

		this.furnaceLabel = new h2d.Text(Assets.bodyFont2x);
		this.furnaceLabel.putBelow(this.tempBar, [0, 10]);
		this.furnaceLabel.text = "Burning";
		this.furnaceLabel.textColor = Constants.ColorsGrey[0];
		this.addChild(this.furnaceLabel);

		this.addChild(this.furnaceBar = new FurnaceBar(this.os));
		this.furnaceBar.putBelow(this.furnaceLabel, [0, 5]);

		this.details = new h2d.Text(Assets.bodyFont2x);
		this.details.putBelow(this.furnaceBar, [0, 10]);
		this.addChild(this.details);
	}

	override public function update(dt: Float) {
		final ship = this.os.ship;
		final newDrain = ship.drain;
		final drainChanged = newDrain != this.energyDrain;
		this.energyDrain = newDrain;

		this.energyBar.update(dt);
		this.tempLabel.text = 'Temperature ${Std.int(ship.temperature)}K';
		this.tempBar.update(dt);
		this.furnaceBar.update(dt);

		final newCharge = ship.charge;

		final netDrain = newCharge - this.energyDrain;

		this.energyLabel.text = [
			'Power Level ',
			'(${Std.int(ship.energy)}/${Std.int(ship.maxCapacity)})',
			' ${netDrain > 0 ? "+" : ""}${netDrain}/s',
		].join("");

		final descriptions = ship.drainDescription;
		this.details.text = descriptions.join("\n");
	}
}
