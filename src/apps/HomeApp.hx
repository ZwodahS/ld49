package apps;

class HomeApp extends App {
	public var apps: Array<AppIcon>;

	public function new(world: OS) {
		super(world);
		this.apps = [];

		var btn: AppIcon = null;
		apps.push(btn = new AppIcon(world, "Metrics", "app:metrics"));
		btn.onClick = function(b: Int) {
			world.switchToApp(new MetricsApp(world));
		}
		apps.push(btn = new AppIcon(world, "Material", "app:material"));
		btn.onClick = function(b: Int) {
			world.switchToApp(new MaterialApp(world));
		}
		apps.push(btn = new AppIcon(world, "Mining", "app:mining"));
		btn.onClick = function(b: Int) {
			world.switchToApp(new MiningApp(world));
		}
		apps.push(btn = new AppIcon(world, "Flight", "app:flight"));
		btn.onClick = function(b: Int) {
			world.switchToApp(new FlightApp(world));
		}
		apps.push(btn = new AppIcon(world, "Guide", "app:help"));
		btn.onClick = function(b: Int) {
			world.switchToApp(new HelpApp(world));
		}
		apps.push(btn = new AppIcon(world, "About", "app:uos"));
		btn.onClick = function(b: Int) {
			world.switchToApp(new CreditApp(world));
		}

		setupUI();
	}

	function setupUI() {
		for (ind => app in this.apps) {
			this.addChild(app);
		}

		resize(Globals.game.gameWidth, Globals.game.gameHeight);
	}

	override public function resize(x: Int, y: Int) {
		for (ind => app in this.apps) {
			app.x = 52 + (ind % 4 * (Constants.AppIconSize.x + 20));
			app.y = 60 + (Std.int(ind / 4) * (Constants.AppIconSize.y + 30));
		}
	}
}
