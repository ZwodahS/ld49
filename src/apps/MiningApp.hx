package apps;

class MiningApp extends App {
	public var surrounding: h2d.Layers;

	public var materials: Array<MiningMaterial>;

	public function new(os: OS) {
		super(os);
		this.materials = [];
		setupUI();
	}

	function setupUI() {
		final title = new h2d.Text(Assets.bodyFont5x);
		title.text = "Surrounding";
		title.setX(Globals.game.gameWidth, AlignCenter).setY(10);
		this.addChild(title);

		final message = [
			"Tap on material to use tractor beam to pull it in.",
			"Doing this will use more power from the ship",
		].join(" ");

		final instruction = new TextBox(message, Globals.game.gameWidth - 20, 60);
		instruction.putBelow(title, [0, 15]).setX(10);
		this.addChild(instruction);
		this.addChild(this.surrounding = new h2d.Layers());

		var ship = Assets.packed.assets.get("ship").getAnim();
		ship.speed = 1;
		this.surrounding.add(ship, 0);

		this.surrounding.x = Globals.game.gameWidth / 2;
		this.surrounding.y = Globals.game.gameHeight / 2 - 50;

		syncMaterials();
	}

	function syncMaterials() {
		final s = this.os.surrounding;
		if (materials.length == 0 && s.visibleMaterials.length == 0) return;

		// @formatter:off
		if (materials.length == s.visibleMaterials.length &&
				materials[0] == s.visibleMaterials[0] &&
				materials[materials.length-1] == s.visibleMaterials[s.visibleMaterials.length-1]) {
			return;
		}

		for (m in this.materials) {
			m.remove();
		}

		this.materials.clear();
		for (m in s.visibleMaterials) {
			this.materials.push(m);
			this.surrounding.addChild(m);
			m.onClick = onClick;
		}
	}

	override public function update(dt: Float) {
		syncMaterials();
	}

	function onClick(m: MiningMaterial) {
		if (this.os.signalStrength <= 0) return;
		if (m.isBeaming) return;
		m.isBeaming = true;
		m.unitVec = [-m.x, -m.y];
		m.unitVec = m.unitVec.unit;
	}
}
