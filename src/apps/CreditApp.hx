package apps;

class CreditApp extends App {
	public static final CreditString = [
		"Thanks for choosing Unstable OS as your primary OS for your ship. In Unstable Corp, we aim to provide the best solution for your every day space needs.",
		"",
		"However, there are some know issues that are we currently working to resolve.",
		"",
		"- Dispose and burn buttons sometimes swap position.",
		// "- Segmentation fault happens when clicking on glitched material.",
		"- Buffer overflow when material is added to a full storage or furnace.",
		"- The wifi sometimes does not work and requires a reboot.",
		"- Some screen flicker is expected. Nothing a reboot can't solve.",
		"- Reboot sometimes take longer than expected.",
		"- And many other bugs.....",
		"",
		"We regret for any inconvenience caused."
	];

	public function new(os: OS) {
		super(os);
		setupUI();
	}

	function setupUI() {
		final title = new h2d.Text(Assets.bodyFont5x);
		title.text = "UnstableOS (Stable Edition)";
		title.setX(Globals.game.gameWidth, AlignCenter).setY(10);
		this.addChild(title);

		final instructions = new h2d.HtmlText(Assets.bodyFont2x);
		var textString = CreditString.join("<br/>");
		instructions.text = textString;
		instructions.maxWidth = Globals.game.gameWidth - 50;
		instructions.putBelow(title, [0, 15]).setX(25);
		instructions.x = 25;
		this.addChild(instructions);
	}
}
