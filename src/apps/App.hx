package apps;

class App extends h2d.Layers {
	public var os: OS;

	public function new(os: OS) {
		super();
		this.os = os;
	}

	public function update(dt: Float) {}

	public function resize(x: Int, y: Int) {}
}
