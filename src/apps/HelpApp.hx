package apps;

class HelpApp extends App {
	public static final HelpString = [
		"Metrics - Give you a good overview of your power",
		"Materials - Give you a good overview of your storage", "Mining - Provide tractor beam interface",
		"Flight - Direct access to flight path information", "",
		"Based on the analysis of your ship, these are some parameters you may want to know.",
		"- Engine min/max operation temperature is 2000/4000K",
		'- Engine base level energy drain is ${Constants.ShipDefaultEnergyDrain}/s',
		'- Tractor Beam cost 0.5/s', '- Max Storage Space is 25', '- 10 items can be burned at once',
	];

	public function new(os: OS) {
		super(os);
		setupUI();
	}

	function setupUI() {
		final title = new h2d.Text(Assets.bodyFont5x);
		title.text = "Guide";
		title.setX(Globals.game.gameWidth, AlignCenter).setY(10);
		this.addChild(title);

		final instructions = new h2d.HtmlText(Assets.bodyFont2x);
		var textString = HelpString.join("<br/>");
		instructions.text = textString;
		instructions.maxWidth = Globals.game.gameWidth - 50;
		instructions.putBelow(title, [0, 15]).setX(25);
		instructions.x = 25;
		this.addChild(instructions);
	}
}
