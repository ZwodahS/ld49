package apps;

import zf.ui.GenericButton;

class MaterialApp extends App {
	var grid: MaterialGrid;

	var helpBox: TextBox;
	var buttons: h2d.Layers;

	var disposeButton: GenericButton;
	var burnButton: GenericButton;

	public function new(os: OS) {
		super(os);
		setupUI();
	}

	function setupUI() {
		final title = new h2d.Text(Assets.bodyFont5x);
		title.text = "Storage";
		title.setX(Globals.game.gameWidth, AlignCenter).setY(10);
		this.addChild(title);

		final message = [
			"Tap on material to see more details and perform action on it.",
			"Selecting a material will select all connected materials",
		].join(" ");

		final instruction = new TextBox(message, Globals.game.gameWidth - 20, 60);
		instruction.putBelow(title, [0, 15]).setX(10);
		this.addChild(instruction);

		final ship = this.os.ship;
		this.grid = new MaterialGrid(ship.storage);

		grid.putBelow(instruction, [0, 20]).setX(Globals.game.gameWidth, AlignCenter);
		grid.onSelect = onSelect;

		final bg = Assets.packed.assets.get("storagebg").getBitmap();
		this.addChild(bg);
		bg.x = grid.x - 2;
		bg.y = grid.y - 2;

		this.addChild(grid);

		this.addChild(this.helpBox = new TextBox("", Globals.game.gameWidth - 20, 80));
		this.helpBox.putBelow(bg, [0, 15]).setX(instruction.x);
		this.helpBox.visible = false;

		this.buttons = new h2d.Layers();
		this.buttons.putBelow(this.helpBox, [0, 20]).setX(0);
		this.addChild(this.buttons);

		this.burnButton = GenericButton.fromTileColors(Assets.packed.assets.get("btn:generic").getTile(),
			[Constants.ColorsWhite[0]]);
		burnButton.font = Assets.bodyFont4x;
		burnButton.text = "Burn";
		burnButton.onClick = function(b: Int) {
			if (this.os.signalStrength <= 0) return;
			final selectedMaterials = this.grid.selectedMaterials;
			this.grid.deselect();
			if (!this.os.ship.burnMaterials(selectedMaterials)) {
				this.os.open_bsod("FURNACE_BUFFER_OVERFLOW", "STOP: 0xUNSTABLE_BO_0000012C");
			}
		}
		burnButton.textLabel.y -= 2;
		burnButton.setX(Globals.game.gameWidth, AlignCenter);
		this.buttons.addChild(burnButton);

		this.disposeButton = GenericButton.fromTileColors(Assets.packed.assets.get("btn:generic").getTile(),
			[Constants.ColorsWhite[0]]);
		disposeButton.font = Assets.bodyFont4x;
		disposeButton.text = "Dispose";
		disposeButton.onClick = function(b: Int) {
			if (this.os.signalStrength <= 0) return;
			final selectedMaterials = this.grid.selectedMaterials;
			this.grid.deselect();
			this.os.ship.disposeMaterials(selectedMaterials);
		}
		disposeButton.textLabel.y -= 2;
		disposeButton.putBelow(burnButton, [0, 10]).setX(Globals.game.gameWidth, AlignCenter);
		this.buttons.addChild(disposeButton);

		this.buttons.visible = false;
	}

	function onSelect(materials: Array<Material>) {
		if (materials == null || materials.length == 0) {
			this.helpBox.visible = false;
			this.buttons.visible = false;
			return;
		}
		this.buttons.visible = true;
		this.helpBox.visible = true;
		this.helpBox.text.text = materials[0].description;

		if (this.os.r.randomChance(Constants.ButtonSwapChance)) {
			final oldP: Point2f = [this.burnButton.x, this.burnButton.y];
			this.burnButton.x = this.disposeButton.x;
			this.burnButton.y = this.disposeButton.y;
			this.disposeButton.x = oldP.x;
			this.disposeButton.y = oldP.y;
		}
	}

	override public function update(dt: Float) {
		this.grid.syncStorage();
	}
}
