package apps;

class FlightApp extends App {
	public static final ShipStart = 350;

	public var ship: h2d.Bitmap;

	public function new(os: OS) {
		super(os);
		setupUI();
	}

	function setupUI() {
		final title = new h2d.Text(Assets.bodyFont5x);
		title.text = "Flight Plan";
		title.setX(Globals.game.gameWidth, AlignCenter).setY(10);
		this.addChild(title);

		this.ship = Assets.packed.assets.get("ship").getBitmap();
		this.addChild(ship);

		var home = Assets.packed.assets.get("homeplanet").getBitmap();
		this.addChild(home);
		home.setX(Globals.game.gameWidth, AlignCenter).setY(45);
		this.ship.setX(Globals.game.gameWidth / 2);
		this.ship.y = ShipStart - (this.os.ship.delta / Constants.TotalTime * ShipStart) + 85;

		var mars = Assets.packed.assets.get("mars").getBitmap();
		this.addChild(mars);
		mars.setX(Globals.game.gameWidth, AlignCenter, -90).setY(230);
	}

	override public function update(dt: Float) {
		this.ship.y = ShipStart - (this.os.ship.delta / Constants.TotalTime * ShipStart) + 85;
	}
}
