class BSOD extends h2d.Layers {
	public static final BSODStrings = [
		"A problem has been detected and UOS has been shutdown to prevent damage to your phone.",
		"::errorString::", "",
		"If this is the first time you've seen this error screen, restart your phone.", "",
		"If you are on a planet, do check for updates and the problem will resolve itself.", "", "",
		"If problems continue, please bring your phone to the Unstable Corp main office on Planet Earth and we will rectify the problem immediately.",
		"", "", "Technical Information:", "*** ::errorCode:: ***",
	];

	public var bootDelay: Float = 3;

	var delta: Float = 0;

	public var done = false;

	var logo: h2d.Bitmap;

	var errorString: String;
	var errorCode: String;

	public function new(errorString: String, errorCode: String) {
		super();
		this.errorString = errorString;
		this.errorCode = errorCode;
		setupUI();
	}

	function setupUI() {
		final bg = Assets.fromColor(Constants.ColorsBlue[1], Globals.game.gameWidth, Globals.game.gameHeight);
		this.addChild(bg);

		final instructions = new h2d.HtmlText(Assets.bodyFont2x);
		var textTemplate = new haxe.Template(BSODStrings.join("<br/>"));
		var textString = textTemplate.execute({errorString: this.errorString, errorCode: this.errorCode});
		instructions.text = textString;
		instructions.maxWidth = Globals.game.gameWidth - 50;
		instructions.setY(25).setX(25);
		instructions.x = 25;
		this.addChild(instructions);
	}

	public function update(dt: Float) {
		delta += dt;
		if (delta > bootDelay) this.done = true;
	}
}
